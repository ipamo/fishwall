#!/bin/sh
iptables  -A INPUT -j ACCEPT -p tcp -m multiport --dports 80,443
ip6tables -A INPUT -j ACCEPT -p tcp -m multiport --dports 80,443
