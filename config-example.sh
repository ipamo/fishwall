#!/bin/sh
# Location of custom-rules directories. Files in this directory whose name ends
# with ".sh" are executed when fw enable command is launched.
#CUSTOM_RULES_DIR=custom

# Allow traffic by default, either with all destinations (*), or with a
# comma-separated list of destinations (for example: "192.168.1.1, 192.168.2.1").
# Note: variables with "6" apply on IPv6.
SSH_IN=*
#SSH6_IN=*

DNS_OUT=*
#DNS6_OUT=*

ICMP_OUT=*
#ICMP6_OUT=*

#TRUST_INOUT="92.222.184.0/24, 92.222.185.0/24, 92.222.186.0/24, 167.114.37.0/24" # See https://docs.ovh.com/fr/dedicated/monitoring-ip-ovh/
#TRUST6_INOUT=

# Allow outgoing emails from given postfix user
#POSTFIX_OUT_USER=postfix

# Apply options "fw -i", "fw -o" and/or "fw -v" by default
#IN_LEVEL=1
#OUT_LEVEL=1
#VERBOSE=1

# Disable management of DOCKER-USER chain
#MANAGE_DOCKER_USER=0
