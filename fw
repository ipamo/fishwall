#!/bin/sh
print_help() {
    echo "FishWall: a firewall for Linux based on the shell.

fw [enable]     Apply firewall rules.
    -o|--out        Allow extended outgoing traffic: http(s), ssh and git ports.
                    Add twice to allow all outgoing traffic.
    -i|--in         Allow extended incoming traffic: http(s), ssh and git ports.
                    Add twice to allow all incoming traffic.
    --log           Log non-filtered packets (for debugging). To see the logs:
                    sudo tail -f /var/log/syslog

fw disable      Remove firewall rules.
    -f|--full       Include non-managed chains

fw status       View current rules.
    -f|--full       Include nat, mangle and IPv6

fw -h|--help    Display help message.
"
}

# -----------------------------------------------------------------------------
# General defines and dependencies
#
ME=$(readlink -f "$0")
DIR=$(dirname "$ME")

. "$DIR/src/include.sh"

[ -f "$DIR/config.sh" ] && . "$DIR/config.sh"

if [ -z "$MANAGE_DOCKER_USER" ]; then
    if chain_exists DOCKER-USER; then
        MANAGE_DOCKER_USER=1
    fi
fi

# -----------------------------------------------------------------------------
# Run command
#
NAME=fw

if [ -z "$1" ]; then
	. "$DIR/src/cmd/enable.sh"
elif [ "$1" = "enable" ]; then
    shift
    . "$DIR/src/cmd/enable.sh"
elif [ "$1" = "disable" ] || [ "$1" = "rm" ]; then
    shift
    . "$DIR/src/cmd/disable.sh"
elif [ "$1" = "status" ] || [ "$1" = "ls" ]; then
    shift
    . "$DIR/src/cmd/status.sh"
elif [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
    print_help
    exit
elif [ "${1%${1#?}}"x = '-x' ] ; then
    # any other word starting with '-': we pass the entire command-line to enable
    . "$DIR/src/cmd/enable.sh"
else
    echo "$NAME: invalid command: $cmd" >&2
    exit 1
fi
