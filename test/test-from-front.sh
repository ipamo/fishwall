#!/bin/sh
set -x

# Through reverse proxy
curl -fsS -m 1 http://revprox
curl -fsS -m 1 http://revprox/front2/
curl -fsS -m 1 http://revprox/back2/

# Direct access
curl -fsS -m 1 http://front2
curl -fsS -m 1 http://172.30.42.102 # back2: should NOT be ok (box => back1 not declared in fw custom rules)

# Access to web
curl -fsS -m 1 http://ip-api.com/line?fields=query # should be ok only if fw was started with option -o
