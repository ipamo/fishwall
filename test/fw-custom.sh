#
# Custom rules for Fishwall.
#

# testfront/revprox => testback/back1
iptables -I DOCKER-USER -j ACCEPT -s 172.30.41.100 -d 172.30.42.101 -p tcp --dport 80 -m comment --comment "testfront/revprox => testback/back1"

# testfront/revprox => testback/back2
iptables -I DOCKER-USER -j ACCEPT -s 172.30.41.100 -d 172.30.42.102 -p tcp --dport 80 -m comment --comment "testfront/revprox => testback/back2"
