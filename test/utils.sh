#
# Shortcuts designed to be sourced in your shell
#
front="docker-compose -p testfront -f docker-compose-front.yml"
back="docker-compose -p testback -f docker-compose-back.yml"

#
# Get container ip address(es). Return non-zero if does not exist.
#
docker_container_ip() {
    local name="$1"
    docker container inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' "$name" 2>/dev/null
}

#
# Check if docker network exists. Return non-zero if not.
#
docker_network_exists() {
    local name="$1"
    docker network inspect -f "{{.Name}}" "$name" >/dev/null 2>&1
}

#
# Get network subnet. Return non-zero if not.
#
docker_network_subnet() {
    local name="$1"
    docker network inspect -f '{{range .IPAM.Config}}{{.Subnet}}{{end}}' "$name" 2>/dev/null
}

inspect_all() {
    echo "testfront_default: $(docker_network_subnet testfront_default)"
    echo "testfront_revprox_1: $(docker_container_ip testfront_revprox_1)"
    echo ""
    echo "testback_default: $(docker_network_subnet testback_default)"
    echo "testback_box_1: $(docker_container_ip testback_box_1)"
    echo "testback_back1_1: $(docker_container_ip testback_back1_1)"
    echo "testback_back2_1: $(docker_container_ip testback_back2_1)"
}

docker_user_apply() {
    iptables -F DOCKER-USER

    iptables -A DOCKER-USER -j ACCEPT -s 172.30.41.10 -d 172.30.42.11 -p tcp --dport 80

    iptables -A DOCKER-USER -j ACCEPT -m conntrack --ctstate RELATED,ESTABLISHED

    iptables -A DOCKER-USER -j LOG -m limit --limit 1/sec --log-prefix "debug-du: "  --log-level 4

    iptables -A DOCKER-USER -j RETURN


    iptables -A OUTPUT -j LOG -m limit --limit 1/sec --log-prefix "debug-out: "  --log-level 4
    iptables -A INPUT -j LOG -m limit --limit 1/sec --log-prefix "debug-in: "  --log-level 4
}
