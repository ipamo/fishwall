#!/bin/sh
set -x

# Direct access
curl -fsS -m 1 http://localhost:4112
curl -fsS -m 1 http://localhost:4212

# Through reverse proxy
curl -fsS -m 1 http://localhost:4100
curl -fsS -m 1 http://localhost:4100/front2/
curl -fsS -m 1 http://localhost:4100/back2/
