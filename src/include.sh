#!/bin/sh

#
# Exit if not root.
#
assert_root() {
	if [ $(id -u) -ne 0 ]; then
		echo "This command must be run as root" >&2
		exit 1
	fi
}

#
# Indicate whether the given iptables chain exist
#
chain_exists() {
    local chain_name="$1" ; shift
    [ $# -eq 1 ] && local table="--table $1"
    iptables $table -n --list "$chain_name" >/dev/null 2>&1
}

#
# Reset chains.
#
reset_standard_chains() {
	iptables -P INPUT ACCEPT
	iptables -F INPUT

	ip6tables -P INPUT ACCEPT
	ip6tables -F INPUT
	
	iptables -P OUTPUT ACCEPT
	iptables -F OUTPUT

	ip6tables -P OUTPUT ACCEPT
	ip6tables -F OUTPUT

	# FORWARD chains are not flushed by default (content of FORWARD chains is not managed by fw)
	iptables -P FORWARD ACCEPT
	ip6tables -P FORWARD ACCEPT
}
