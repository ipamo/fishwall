#!/bin/sh
#
# "fw status" command, sourced from within main script.
#

# -----------------------------------------------------------------------------
# Parse options
#
NAME=fw-status
options=$(getopt -n "$NAME" -o "f" -l "full" -- "$@")
[ $? -ne 0 ] && exit 1
eval set -- "$options"

while true; do
    case "$1" in
        -f|--full)      FULL=1 ;;
        # ----- end of option list -----
        --) shift; break ;;
        *)  echo "$NAME: unhandled option: $1" >&2; exit 1 ;;
    esac
    shift # option name
done

# -----------------------------------------------------------------------------
# Run command
#
assert_root

if [ "$FULL" = "1" ]; then	
    echo "
-------------------------------------------------------------------------------
filter, v4
"
fi

iptables -nvL --line-numbers

if [ "$FULL" = "1" ]; then
    echo "
-------------------------------------------------------------------------------
filter, v6
"
    ip6tables -nvL --line-numbers

    echo "
-------------------------------------------------------------------------------
nat, v4
"
    iptables -t nat -nvL --line-numbers

    echo "
-------------------------------------------------------------------------------
nat, v6
"
    ip6tables -t nat -nvL --line-numbers

    echo "
-------------------------------------------------------------------------------
mangle, v4
"
    iptables -t mangle -nvL --line-numbers

    echo "
-------------------------------------------------------------------------------
filter, v6
"
    ip6tables -t mangle -nvL --line-numbers
fi
