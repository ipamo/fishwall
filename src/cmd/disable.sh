#!/bin/sh
#
# "fw disable" command, sourced from within main script.
#

# -----------------------------------------------------------------------------
# Parse options
#
NAME=fw-disable
options=$(getopt -n "$NAME" -o "f" -l "full" -- "$@")
[ $? -ne 0 ] && exit 1
eval set -- "$options"

while true; do
    case "$1" in
        -f|--full)      FULL=1 ;;
        # ----- end of option list -----
        --) shift; break ;;
        *)  echo "$NAME: unhandled option: $1" >&2; exit 1 ;;
    esac
    shift # option name
done

# -----------------------------------------------------------------------------
# Run command
#
assert_root

reset_standard_chains

if [ "$MANAGE_DOCKER_USER" = "1" ]; then
    iptables -F DOCKER-USER
    iptables -A DOCKER-USER -j RETURN
fi

if [ "$FULL" = "1" ]; then
    # Flush FORWARD chains (not performed in reset_chains)
    iptables -F FORWARD
    ip6tables -F FORWARD

    iptables -t nat -F
    iptables -t nat -X
    iptables -t mangle -F
    iptables -t mangle -X
    iptables -F
    iptables -X

    ip6tables -t nat -F
    ip6tables -t nat -X
    ip6tables -t mangle -F
    ip6tables -t mangle -X
    ip6tables -F
    ip6tables -X

    if [ "$MANAGE_DOCKER_USER" = "1" ]; then
        echo "Reinstall docker isolation rules with: sudo service docker restart" >&2
    fi
fi
