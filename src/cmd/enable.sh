#!/bin/sh
#
# "fw enable" command, sourced from within main script.
#

# -----------------------------------------------------------------------------
# Parse options
#
NAME=fw-enable
options=$(getopt -n "$NAME" -o "ioav" -l "in,out,all,log,verbose" -- "$@")
[ $? -ne 0 ] && exit 1
eval set -- "$options"

[ -z "$IN_LEVEL" ] && IN_LEVEL=0
[ -z "$OUT_LEVEL" ] && OUT_LEVEL=0
[ -z "$VERBOSE" ] && VERBOSE=0
while true; do
    case "$1" in
        -i|--in)       IN_LEVEL=$(($IN_LEVEL+1)) ;;
        -o|--out)      OUT_LEVEL=$(($OUT_LEVEL+1)) ;;
        -v|--verbose)  VERBOSE=$(($VERBOSE+1)) ;;
        --log)         LOG_NON_FILTERED=1 ;;
        # ----- end of option list -----
        --) shift; break ;;
        *)  echo "$NAME: unhandled option: $1" >&2; exit 1 ;;
    esac
    shift # option name
done

# -----------------------------------------------------------------------------
# Run command
#
assert_root

reset_standard_chains
[ "$MANAGE_DOCKER_USER" = "1" ] && iptables -F DOCKER-USER

# Keep established connections
iptables  -A INPUT -j ACCEPT -m conntrack --ctstate RELATED,ESTABLISHED
ip6tables -A INPUT -j ACCEPT -m conntrack --ctstate RELATED,ESTABLISHED

iptables  -A OUTPUT -j ACCEPT -m conntrack --ctstate RELATED,ESTABLISHED
ip6tables -A OUTPUT -j ACCEPT -m conntrack --ctstate RELATED,ESTABLISHED

[ "$MANAGE_DOCKER_USER" = "1" ] && iptables -A DOCKER-USER -j ACCEPT -m conntrack --ctstate RELATED,ESTABLISHED

# Set default policy to DROP
iptables  -P INPUT DROP
ip6tables -P INPUT DROP

iptables  -P OUTPUT DROP
ip6tables -P OUTPUT DROP

iptables  -P FORWARD DROP
ip6tables -P FORWARD DROP

# Allow loopback connections
iptables  -A INPUT -j ACCEPT -i lo
ip6tables -A INPUT -j ACCEPT -i lo

iptables  -A OUTPUT -j ACCEPT -o lo
ip6tables -A OUTPUT -j ACCEPT -o lo

# Allow communication from localhost to all Docker bridge networks
if [ "$MANAGE_DOCKER_USER" = "1" ]; then
    for net_id in $(docker network ls -q -f "driver=bridge"); do
        net_subnet=$(docker network inspect -f '{{range .IPAM.Config}}{{.Subnet}}{{end}}' "$net_id")
        if [ -n "$net_subnet" ]; then
            net_iface=$(ip route show "$net_subnet" | cut -d ' ' -f 3)
            if [ $VERBOSE -ge 1 ]; then
                net_name=$(docker network inspect -f '{{.Name}}' "$net_id")
                echo "Accept input/output from/to Docker interface: $net_iface (subnet: $net_subnet, name: $net_name, id: $net_id)"
            fi

            iptables  -A INPUT -j ACCEPT -i "$net_iface"
            iptables  -A OUTPUT -j ACCEPT -o "$net_iface"
        fi
    done
fi

# Allow outgoing ping
if [ -n "$ICMP_OUT" ]; then
    if [ "$ICMP_OUT" = "*" ]; then
        iptables  -A OUTPUT -j ACCEPT -p icmp

        [ "$MANAGE_DOCKER_USER" = "1" ] && iptables -A DOCKER-USER -j ACCEPT -s 172.16.0.0/12 -p icmp
    else
        iptables  -A OUTPUT -j ACCEPT -p icmp -d "$ICMP_OUT"

        [ "$MANAGE_DOCKER_USER" = "1" ] && iptables -A DOCKER-USER -j ACCEPT -s 172.16.0.0/12 -p icmp -d "$ICMP_OUT"
    fi
fi

if [ -n "$ICMP6_OUT" ]; then
    if [ "$ICMP6_OUT" = "*" ]; then
        ip6tables -A OUTPUT -j ACCEPT -p icmp
    else
        ip6tables -A OUTPUT -j ACCEPT -p icmp -d "$ICMP6_OUT"
    fi
fi

# Allow outgoing DNS
if [ -n "$DNS_OUT" ]; then
    if [ "$DNS_OUT" = "*" ]; then
        iptables  -A OUTPUT -j ACCEPT -p udp --dport 53

        [ "$MANAGE_DOCKER_USER" = "1" ] && iptables -A DOCKER-USER -j ACCEPT -s 172.16.0.0/12 -p udp --dport 53
    else
        iptables  -A OUTPUT -j ACCEPT -p udp --dport 53 -d "$DNS_OUT"

        [ "$MANAGE_DOCKER_USER" = "1" ] && iptables -A DOCKER-USER -j ACCEPT -s 172.16.0.0/12 -p udp --dport 53 -d "$DNS_OUT"
    fi
fi

if [ -n "$DNS6_OUT" ]; then
    if [ "$DNS6_OUT" = "*" ]; then
        ip6tables -A OUTPUT -j ACCEPT -p udp --dport 53
    else
        ip6tables -A OUTPUT -j ACCEPT -p udp --dport 53 -d "$DNS6_OUT"
    fi
fi

# Allow outgoing emails (only from postfix user)
if [ -n "$POSTFIX_OUT_USER" ]; then
    iptables -A OUTPUT -j ACCEPT -p tcp --dport 25 -m owner --uid-owner "$POSTFIX_OUT_USER"
    iptables -A OUTPUT -j REJECT -p tcp --dport 25 --reject-with icmp-port-unreachable # reject emails immediatly if they are not sent from postfix user
fi

# Allow incoming ssh
if [ -n "$SSH_IN" ]; then
    if [ "$SSH_IN" = "*" ]; then
        iptables  -A INPUT -j ACCEPT -p tcp --dport 22
    else
        iptables  -A INPUT -j ACCEPT -p tcp --dport 22 -s "$SSH_IN"
    fi
fi

if [ -n "$SSH6_IN" ]; then
    if [ "$SSH6_IN" = "*" ]; then
        ip6tables -A INPUT -j ACCEPT -p tcp --dport 22
    else
        ip6tables -A INPUT -j ACCEPT -p tcp --dport 22 -s "$SSH6_IN"
    fi
fi

# Allow outgoing ssh
if [ -n "$SSH_OUT" ]; then
    if [ "$SSH_OUT" = "*" ]; then
        iptables  -A OUTPUT -j ACCEPT -p tcp --dport 22
    else
        iptables  -A OUTPUT -j ACCEPT -p tcp --dport 22 -d "$SSH_OUT"
    fi
fi

if [ -n "$SSH6_OUT" ]; then
    if [ "$SSH6_OUT" = "*" ]; then
        ip6tables -A OUTPUT -j ACCEPT -p tcp --dport 22
    else
        ip6tables -A OUTPUT -j ACCEPT -p tcp --dport 22 -d "$SSH6_OUT"
    fi
fi

# Allow everything with trusted networks (such as monitoring IP addresses)
if [ -n "$TRUST_INOUT" ]; then
    iptables -A INPUT -j ACCEPT -s "$TRUST_INOUT"
    iptables -A OUTPUT -j ACCEPT -d "$TRUST_INOUT"
fi

if [ -n "$TRUST6_INOUT" ]; then
    ip6tables -A INPUT -j ACCEPT -s "$TRUST6_INOUT"
    ip6tables -A OUTPUT -j ACCEPT -d "$TRUST6_INOUT"
fi

# Allow extended outgoing traffic
if [ $OUT_LEVEL -ge 1 ]; then
    [ $VERBOSE -ge 1 ] && echo "Extended OUT level: $OUT_LEVEL"

    if [ $OUT_LEVEL -ge 2 ]; then
        iptables  -A OUTPUT -j ACCEPT -m comment --comment "out_level_2"
        ip6tables -A OUTPUT -j ACCEPT -m comment --comment "out_level_2"

        [ "$MANAGE_DOCKER_USER" = "1" ] && iptables -A DOCKER-USER -j ACCEPT -s 172.16.0.0/12 -m comment --comment "out_level_2"
    else
        iptables  -A OUTPUT -j ACCEPT -p tcp -m multiport --dports 22,80,443,9418 -m comment --comment "out_level_1"
        ip6tables -A OUTPUT -j ACCEPT -p tcp -m multiport --dports 22,80,443,9418 -m comment --comment "out_level_1"

        [ "$MANAGE_DOCKER_USER" = "1" ] && iptables -A DOCKER-USER -j ACCEPT -s 172.16.0.0/12 -p tcp -m multiport --dports 22,80,443,9418 -m comment --comment "out_level_1"
    fi
fi

# Allow extended incoming traffic
if [ $IN_LEVEL -ge 1 ]; then
    [ $VERBOSE -ge 1 ] && echo "Extended IN level: $IN_LEVEL"

    if [ $IN_LEVEL -ge 2 ]; then
        iptables  -A INPUT -j ACCEPT -m comment --comment "in_level_2"
        ip6tables -A INPUT -j ACCEPT -m comment --comment "in_level_2"
    else
        iptables  -A INPUT -j ACCEPT -p tcp -m multiport --sports 22,80,443,9418 -m comment --comment "in_level_1"
        ip6tables -A INPUT -j ACCEPT -p tcp -m multiport --sports 22,80,443,9418 -m comment --comment "in_level_1"
    fi
fi

# Add custom rules
if [ -n "$CUSTOM_RULES_DIR" ]; then
    if [ ! "${CUSTOM_RULES_DIR%${CUSTOM_RULES_DIR#?}}"x = '/x' ] ; then
        # absolutize CUSTOM_RULES_DIR
        CUSTOM_RULES_DIR=$DIR/$CUSTOM_RULES_DIR
    fi

    if [ -d "$CUSTOM_RULES_DIR" ]; then
        for custom in `ls -1 $CUSTOM_RULES_DIR/*.sh 2>/dev/null`; do
            # Source custom file
            [ $VERBOSE -ge 1 ] && echo "Source custom file: $custom"
            . "$custom"
        done
    else
        echo "CUSTOM_RULES_DIR does not exist: $CUSTOM_RULES_DIR" >&2
    fi
fi

# Enable logging of non-filtered packets (for debugging)
if [ "$LOG_NON_FILTERED" = "1" ]; then
    limit=2/min

    iptables  -A INPUT -j LOG -m limit --limit "$limit" --log-prefix "fw-in: "  --log-level 4
    ip6tables -A INPUT -j LOG -m limit --limit "$limit" --log-prefix "fw6-in: " --log-level 4

    iptables  -A OUTPUT -j LOG -m limit --limit "$limit" --log-prefix "fw-out: "  --log-level 4
    ip6tables -A OUTPUT -j LOG -m limit --limit "$limit" --log-prefix "fw6-out: " --log-level 4

    [ "$MANAGE_DOCKER_USER" = "1" ] && iptables -A DOCKER-USER -j LOG -m limit --limit "$limit" --log-prefix "fw-du: "  --log-level 4
fi

if [ "$MANAGE_DOCKER_USER" ]; then
    # Remove outgoing traffic from Docker containers to outside (except exceptions above)
    iptables -A DOCKER-USER -j DROP -s 172.16.0.0/12 ! -d 172.16.0.0/12

    iptables -A DOCKER-USER -j RETURN
fi
