Fishwall
========

A firewall for Linux based on the shell.

Built on top of netfilter (`iptables` and `ip6tables` interfaces).

Compatible with Docker: Fishwall manage content of `DOCKER-USER` chain, and does
not alter content of `FORWARD` chain.

## Install

Create config file:

	cp config.example.sh config.sh

Add to PATH:

    sudo ln -s /full/path/to/repository/fw /usr/local/bin/

Execute automatically at startup:

    sudo cp fw.service /etc/systemd/system/
    sudo systemctl daemon-reload
    sudo systemctl enable fw
	
Alternative on Debian (warning: probably does not work with Docker because
DOCKER-USER chain is probably not fully created when if-up.d is executed):

	sudo -s fw /full/path/to/repository/fw /etc/network/if-up.d/

## Usage

	sudo fw -h

## Test

```sh
# Prepare fw
[ -f "config.sh" ] || cp config-example.sh config.sh
mkdir -p custom
ln -s ../test/fw-custom.sh custom/test.sh
sudo ln -s "$(realpath fw)" /usr/local/bin/

# Prepare test
cd test/
. utils.sh

$back up -d
$front up -d

# Necessary when docker network(s) changed
sudo fw -v 

./test-from-host.sh

$front run box ./test-from-front.sh
```

## License

Published under the [MIT license](LICENSE.txt).
